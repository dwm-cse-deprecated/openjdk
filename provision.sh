#! /bin/bash

set -ex

yum -y install java-1.8.0-openjdk java-1.8.0-openjdk-devel

echo "export JAVA_HOME=/usr/lib/jvm/java-1.8.0-openjdk.x86_64" > /etc/profile.d/java.sh
